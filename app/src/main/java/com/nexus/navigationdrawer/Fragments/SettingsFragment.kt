package com.nexus.navigationdrawer.Fragments

import android.app.AlertDialog
import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.View.LAYOUT_DIRECTION_LTR
import android.view.View.LAYOUT_DIRECTION_RTL
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.nexus.navigationdrawer.R
import kotlinx.android.synthetic.main.settings_fragment.*
import kotlinx.android.synthetic.main.theme_dialog.view.*
import com.nexus.navigationdrawer.Activities.MainActivity
import com.nexus.navigationdrawer.Activities.SharedPreference
import com.nexus.navigationdrawer.Helper.LocaleHelper
import io.paperdb.Paper
import kotlinx.android.synthetic.main.nav_header_main.*


//import javax.swing.text.StyleConstants.getBackground





@Suppress("DEPRECATION")
class SettingsFragment : Fragment() {
    var mainActivity = MainActivity()
    //val sharedPreference:SharedPreference= SharedPreference(requireContext())
//    var contextThemeWrapper = ContextThemeWrapper(requireContext(),R.style.brown_theme )


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //returning our layout file
        //val contextThemeWrapper = ContextThemeWrapper(requireContext(),R.style.brown_theme )
        // clone the inflater using the ContextThemeWrapper
        //      val localInflater = inflater.cloneInContext(contextThemeWrapper)
        //    return localInflater.inflate(R.layout.settings_fragment,container,false)


        return inflater.inflate(R.layout.settings_fragment, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Paper.init(view.context)
        var check:String?=Paper.book().read<String>("language")
        if (check=="ur") {
            language_button.text = "Urdu"

        }
        if(check=="en")
        {
            language_button.text = "English"
        }
        if (check==null)
        {
            Paper.book().write("language","en")
            language_button.text="English"
        }
        //you can set the title for your toolbar here for different fragments different titles
        activity!!.title=resources.getString(R.string.setting_frag_title)
        val languages = resources.getStringArray(R.array.languages_arrays)
        //val spinner = view.findViewById<Spinner>(R.id.spinner_language)
        language_button.setOnClickListener {
            languageButtonListener()
        }

/*
        if (spinner != null) {
            //for selected language

            val adapter = ArrayAdapter(
                requireContext(),
                android.R.layout.simple_spinner_item, languages
            )

            spinner.adapter = adapter
            spinner.setPrompt("Select Language")


            spinner.setSelection(0,false)
            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    if (position == 1) {
                        Toast.makeText(
                            requireContext(),
                            "You Selected Urdu Language", Toast.LENGTH_SHORT
                        ).show()
                        //spinner.setSelection(URDU_LANGUAGE)
                        //mainActivity.updateView(Paper.book().read<String>("language"))
                        Paper.init(requireContext())
                        Paper.book().write("language","ur")
                        updateView(Paper.book().read<String>("language"))
                        mainActivity.restartActivity(requireActivity())
                    }
                    if (position == 0) {
                        Toast.makeText(
                            requireContext(),
                            "You Selected English Language", Toast.LENGTH_SHORT
                        ).show()
                        Paper.init(requireContext())
                        Paper.book().write("language","en")
                        updateView(Paper.book().read<String>("language"))
                        //spinner.setSelection(ENGLISH_LANGUAGE)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    spinner.setSelection(0)
                }
            }
        }
*/

        //handling theme button
        btn_select_theme.setOnClickListener {

            val sharedPreference:SharedPreference=SharedPreference(requireContext())
            if(sharedPreference.getValueInt("Theme")==mainActivity.THEME_BLUE)
                btn_select_theme.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorBlue)))
            if(sharedPreference.getValueInt("Theme")==mainActivity.THEME_GREEN)
                btn_select_theme.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorGreen)))
            if(sharedPreference.getValueInt("Theme")==mainActivity.THEME_BROWN)
                btn_select_theme.setBackgroundColor(resources.getColor(R.color.colorBrown))
            //Inflate the dialog with custom view
            val mDialogView =
                LayoutInflater.from(requireContext()).inflate(R.layout.theme_dialog, null)
            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(requireContext())
                .setView(mDialogView)

            //show dialog
            val mAlertDialog = mBuilder.show()

            /////for theme 1
            mDialogView.layout1.setOnClickListener(View.OnClickListener {
                mDialogView.first_theme.visibility = View.VISIBLE
                mDialogView.second_theme.visibility = View.INVISIBLE
                mDialogView.third_theme.visibility = View.INVISIBLE
                mDialogView.fourth_theme.visibility = View.INVISIBLE
                mDialogView.fifth_theme.visibility = View.INVISIBLE
                mDialogView.sixth_theme.visibility = View.INVISIBLE
                mDialogView.sevent_theme.visibility = View.INVISIBLE
                mDialogView.eighth_theme.visibility = View.INVISIBLE

            })
            /////for theme 2
            mDialogView.layout2.setOnClickListener(View.OnClickListener {
                mDialogView.first_theme.visibility = View.INVISIBLE
                mDialogView.second_theme.visibility = View.VISIBLE
                mDialogView.third_theme.visibility = View.INVISIBLE
                mDialogView.fourth_theme.visibility = View.INVISIBLE
                mDialogView.fifth_theme.visibility = View.INVISIBLE
                mDialogView.sixth_theme.visibility = View.INVISIBLE
                mDialogView.sevent_theme.visibility = View.INVISIBLE
                mDialogView.eighth_theme.visibility = View.INVISIBLE
            })
            /////for theme 3
            mDialogView.layout3.setOnClickListener(View.OnClickListener {
                mDialogView.first_theme.visibility = View.INVISIBLE
                mDialogView.second_theme.visibility = View.INVISIBLE
                mDialogView.third_theme.visibility = View.VISIBLE
                mDialogView.fourth_theme.visibility = View.INVISIBLE
                mDialogView.fifth_theme.visibility = View.INVISIBLE
                mDialogView.sixth_theme.visibility = View.INVISIBLE
                mDialogView.sevent_theme.visibility = View.INVISIBLE
                mDialogView.eighth_theme.visibility = View.INVISIBLE
            })
            /////for theme 4
            mDialogView.layout4.setOnClickListener(View.OnClickListener {
                mDialogView.first_theme.visibility = View.INVISIBLE
                mDialogView.second_theme.visibility = View.INVISIBLE
                mDialogView.third_theme.visibility = View.INVISIBLE
                mDialogView.fourth_theme.visibility = View.VISIBLE
                mDialogView.fifth_theme.visibility = View.INVISIBLE
                mDialogView.sixth_theme.visibility = View.INVISIBLE
                mDialogView.sevent_theme.visibility = View.INVISIBLE
                mDialogView.eighth_theme.visibility = View.INVISIBLE
            })

            /////for theme 5
            mDialogView.layout5.setOnClickListener(View.OnClickListener {
                mDialogView.first_theme.visibility = View.INVISIBLE
                mDialogView.second_theme.visibility = View.INVISIBLE
                mDialogView.third_theme.visibility = View.INVISIBLE
                mDialogView.fourth_theme.visibility = View.INVISIBLE
                mDialogView.fifth_theme.visibility = View.VISIBLE
                mDialogView.sixth_theme.visibility = View.INVISIBLE
                mDialogView.sevent_theme.visibility = View.INVISIBLE
                mDialogView.eighth_theme.visibility = View.INVISIBLE
            })
            /////for theme 6
            mDialogView.layout6.setOnClickListener(View.OnClickListener {
                mDialogView.first_theme.visibility = View.INVISIBLE
                mDialogView.second_theme.visibility = View.INVISIBLE
                mDialogView.third_theme.visibility = View.INVISIBLE
                mDialogView.fourth_theme.visibility = View.INVISIBLE
                mDialogView.fifth_theme.visibility = View.INVISIBLE
                mDialogView.sixth_theme.visibility = View.VISIBLE
                mDialogView.sevent_theme.visibility = View.INVISIBLE
                mDialogView.eighth_theme.visibility = View.INVISIBLE
            })
            /////for theme 7
            mDialogView.layout7.setOnClickListener(View.OnClickListener {
                mDialogView.first_theme.visibility = View.INVISIBLE
                mDialogView.second_theme.visibility = View.INVISIBLE
                mDialogView.third_theme.visibility = View.INVISIBLE
                mDialogView.fourth_theme.visibility = View.INVISIBLE
                mDialogView.fifth_theme.visibility = View.INVISIBLE
                mDialogView.sixth_theme.visibility = View.INVISIBLE
                mDialogView.sevent_theme.visibility = View.VISIBLE
                mDialogView.eighth_theme.visibility = View.INVISIBLE
            })
            /////for theme 8
            mDialogView.layout8.setOnClickListener(View.OnClickListener {
                mDialogView.first_theme.visibility = View.INVISIBLE
                mDialogView.second_theme.visibility = View.INVISIBLE
                mDialogView.third_theme.visibility = View.INVISIBLE
                mDialogView.fourth_theme.visibility = View.INVISIBLE
                mDialogView.fifth_theme.visibility = View.INVISIBLE
                mDialogView.sixth_theme.visibility = View.INVISIBLE
                mDialogView.sevent_theme.visibility = View.INVISIBLE
                mDialogView.eighth_theme.visibility = View.VISIBLE
            })


            //done button click of dialog layout


            mDialogView.btn_done_theme.setOnClickListener {
                //dismiss dialog
                mAlertDialog.dismiss()
                Toast.makeText(requireContext(), "Theme Applied", Toast.LENGTH_SHORT).show()
                if (mDialogView.first_theme.visibility == View.VISIBLE) {
                    btn_select_theme.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorBlue)))

                    val sharedPreference: SharedPreference = SharedPreference(view.context)
                    sharedPreference.save("Theme", mainActivity.THEME_BLUE)
                }

                if (mDialogView.second_theme.visibility == View.VISIBLE) {
                    btn_select_theme.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorPurple)))
                    val sharedPreference: SharedPreference = SharedPreference(view.context)
                    sharedPreference.save("Theme", mainActivity.THEME_PURPULE)

                }
                if (mDialogView.third_theme.visibility == View.VISIBLE) {
                    btn_select_theme.setBackgroundColor(
                        btn_select_theme.context.resources.getColor(
                            R.color.colorGreen
                        )
                    )
                    val sharedPreference: SharedPreference = SharedPreference(view.context)
                    sharedPreference.save("Theme", mainActivity.THEME_GREEN)

                }
                if (mDialogView.fourth_theme.visibility == View.VISIBLE) {
                    btn_select_theme.setBackgroundColor(
                        btn_select_theme.context.resources.getColor(
                            R.color.colorRed
                        )
                    )
                    val sharedPreference: SharedPreference = SharedPreference(view.context)
                    sharedPreference.save("Theme", mainActivity.THEME_RED)

                }
                if (mDialogView.fifth_theme.visibility == View.VISIBLE) {
                    btn_select_theme.setBackgroundColor(
                        btn_select_theme.context.resources.getColor(
                            R.color.colorOrange
                        )
                    )
                    val sharedPreference: SharedPreference = SharedPreference(view.context)
                    sharedPreference.save("Theme", mainActivity.THEME_ORANGE)

                }
                if (mDialogView.sixth_theme.visibility == View.VISIBLE) {
                    btn_select_theme.setBackgroundColor(
                        btn_select_theme.context.resources.getColor(
                            R.color.colorBrown
                        )
                    )
                    val sharedPreference: SharedPreference = SharedPreference(view.context)
                    sharedPreference.save("Theme", mainActivity.THEME_BROWN)

                }
                if (mDialogView.sevent_theme.visibility == View.VISIBLE) {
                    btn_select_theme.setBackgroundColor(
                        btn_select_theme.context.resources.getColor(
                            R.color.colorMaroon
                        )
                    )
                    val sharedPreference: SharedPreference = SharedPreference(view.context)
                    sharedPreference.save("Theme", mainActivity.THEME_MAROON)
                }
                if (mDialogView.eighth_theme.visibility == View.VISIBLE) {
                    btn_select_theme.setBackgroundColor(
                        btn_select_theme.context.resources.getColor(
                            R.color.colorPlum
                        )
                    )
                    val sharedPreference: SharedPreference = SharedPreference(view.context)
                    sharedPreference.save("Theme", mainActivity.THEME_PLUM)
                }

               mainActivity.restartActivity(requireActivity())
                //GlobalClass.recreateActivityCompat(requireActivity())
            }

        }

    }

    private fun languageButtonListener() {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(requireContext())

        // Set the alert dialog title
        builder.setTitle("Select Language")

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("Urdu"){dialog, which ->
            // Do something when user press the positive button
            Toast.makeText(requireContext(),"You selected Urdu.",Toast.LENGTH_SHORT).show()
            language_button.text="Urdu"
            Paper.book().write("language","ur")
            updateView(Paper.book().read<String>("language"))
            mainActivity.restartActivity(requireActivity())
        }


        // Display a negative button on alert dialog
        builder.setNegativeButton("English"){dialog,which ->
            Toast.makeText(requireContext(),"You selected English.",Toast.LENGTH_SHORT).show()
            language_button.text="English"
            Paper.book().write("language","en")
            updateView(Paper.book().read<String>("language"))
            mainActivity.restartActivity(requireActivity())
        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()

    }

    private fun updateView(lang: String?) {
        var context: Context = LocaleHelper.setLocale(requireContext(),lang)
        //var resources: Resources =context.resources
        //val appName = findViewById<View>(R.id.app_name) as TextView
        //appName.text=(resources.getString(R.string.app_name))
        //app_name?.text=resources.getString(R.string.app_name)
        //textView?.text=resources.getString(R.string.nav_header_subtitle)
        //var item: MenuItem =mainActivity.menuMain.findItem(R.id.nav_home)
        //item.title=resources.getString(R.string.menu_home)
    }


}
