package com.nexus.navigationdrawer.Fragments

import android.app.AlertDialog
import android.content.Context
import android.database.Cursor
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.nexus.navigationdrawer.R
import com.nexus.recyclerviewkotlin.MainAdapter
import com.nexus.recyclerviewkotlin.Model_Video
import com.smarteist.autoimageslider.SliderViewAdapter
import kotlinx.android.synthetic.main.fr_menu_1.*
import kotlinx.android.synthetic.main.fr_menu_1.view.*
import com.bumptech.glide.Glide
import com.esotericsoftware.kryo.util.IntArray
import com.nexus.navigationdrawer.Activities.MainActivity
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import io.paperdb.Paper

class HomeFragment : Fragment() {

    internal var root: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        root= inflater.inflate(R.layout.fr_menu_1, container, false)  //R is changing here
        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //you can set the title for your toolbar here for different fragments different titles
        activity!!.title=resources.getString(R.string.home_frag_title)
        //implementation of sliderview
        var adapter:SliderAdapterExample
        adapter=SliderAdapterExample(requireContext())
        imageSlider.sliderAdapter=adapter

        /////aditional
        imageSlider.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        imageSlider.setIndicatorSelectedColor(Color.WHITE);
        imageSlider.setIndicatorUnselectedColor(Color.GRAY);
        imageSlider.setScrollTimeInSec(3); //set scroll delay in seconds :
        imageSlider.startAutoCycle();
        //////////////
        download_button.setOnClickListener {
            val downloadsFragment = DownloadsFragment()
            val transaction = fragmentManager!!.beginTransaction()
            transaction.replace(
                R.id.nav_host_fragment,
                downloadsFragment
            ) // give your fragment container id in first parameter
            transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
            transaction.commit()
        }
    }

    internal inner class SliderAdapterVH(var itemView: View) :
        SliderViewAdapter.ViewHolder(itemView) {
        var imageViewBackground: ImageView
        var textViewDescription: TextView

        init {
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider)
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider)
        }
    }

    private inner class SliderAdapterExample(private val context: Context) :
        SliderViewAdapter<SliderAdapterExample.SliderAdapterVH>() {


        override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {
            val inflate =
                LayoutInflater.from(parent.context).inflate(R.layout.image_slider_layout_item, null)
            return SliderAdapterVH(inflate)
        }

        override fun onBindViewHolder(viewHolder: SliderAdapterVH, position: Int) {
            viewHolder.textViewDescription.text = "Item # $position"
            viewHolder.imageViewBackground.setOnClickListener(View.OnClickListener {
                categoryDialog(position)
            })
            Paper.init(requireContext())
            var check: String? = Paper.book().read<String>("category")
            if (check == "first_cat")
            {
                when (position) {
                    0 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_one_one)
                        .into(viewHolder.imageViewBackground)
                    1 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_one_two)
                        .into(viewHolder.imageViewBackground)
                    2 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_one_three)
                        .into(viewHolder.imageViewBackground)
                    else -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_one_four)
                        .into(viewHolder.imageViewBackground)
                }
        }
            if (check == "second_cat")
            {
                when (position) {
                    0 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_two_first)
                        .into(viewHolder.imageViewBackground)
                    1 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_two_second)
                        .into(viewHolder.imageViewBackground)
                    2 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_two_third)
                        .into(viewHolder.imageViewBackground)
                    else -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_two_fourth)
                        .into(viewHolder.imageViewBackground)
                }
        }
            if (check == "third_cat")
            {
                when (position) {
                    0 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_three_one)
                        .into(viewHolder.imageViewBackground)
                    1 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_three_two)
                        .into(viewHolder.imageViewBackground)
                    2 -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_three_three)
                        .into(viewHolder.imageViewBackground)
                    else -> Glide.with(viewHolder.itemView)
                        .load(R.drawable.cat_three_fourth)
                        .into(viewHolder.imageViewBackground)
                }
        }

        }

        override fun getCount(): Int {
            //slider view count could be dynamic size
            return 4
        }

        internal inner class SliderAdapterVH(var itemView: View) :
            SliderViewAdapter.ViewHolder(itemView) {
            var imageViewBackground: ImageView
            var textViewDescription: TextView

            init {
                imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider)
                textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider)
                /*imageViewBackground.setOnTouchListener(object:View.OnTouchListener{
                    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                        categoryDialog()
                        return true
                    }
                })*/
            }
        }


    }
    private fun categoryDialog(position: Int) {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(requireContext())

        // Set the alert dialog title
        builder.setTitle("Item #"+position)


        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK"){dialog, which ->
            // Do something when user press the positive button
            dialog.dismiss()
        }


        // Display a negative button on alert dialog
        builder.setNegativeButton("Cancel"){dialog,which ->
            dialog.dismiss()
        }

/*
        //Linear layout
        var layout:LinearLayout= LinearLayout(requireContext())
        var params:LinearLayout.LayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layout.setLayoutParams(params)
            layout.setOrientation(LinearLayout.VERTICAL)
*/

        Paper.init(requireContext())
        var check:String= Paper.book().read<String>("category")
        var img: ImageView = ImageView(requireContext())
        if (check=="first_cat") {
            when (position) {
                0 -> {
                    img.setImageResource(R.drawable.cat_one_one)
                }
                1 -> {
                    img.setImageResource(R.drawable.cat_one_two)
                }
                2 -> {
                    img.setImageResource(R.drawable.cat_one_two)
                }
                3 -> {
                    img.setImageResource(R.drawable.cat_one_three)
                }
            }
        }
        if (check=="second_cat") {
            when (position) {
                0 -> {
                    img.setImageResource(R.drawable.cat_two_first)
                }
                1 -> {
                    img.setImageResource(R.drawable.cat_two_second)
                }
                2 -> {
                    img.setImageResource(R.drawable.cat_two_third)
                }
                3 -> {
                    img.setImageResource(R.drawable.cat_two_fourth)
                }
            }
        }
        if (check=="third_cat") {
            when (position) {
                0 -> {
                    img.setImageResource(R.drawable.cat_three_one)
                }
                1 -> {
                    img.setImageResource(R.drawable.cat_three_two)
                }
                2 -> {
                    img.setImageResource(R.drawable.cat_three_three)
                }
                3 -> {
                    img.setImageResource(R.drawable.cat_three_fourth)
                }
            }
        }


//            layout.addView(img1)
            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()
            dialog.setView(img)
            // Display the alert dialog on app interface
            dialog.show()
        }

}