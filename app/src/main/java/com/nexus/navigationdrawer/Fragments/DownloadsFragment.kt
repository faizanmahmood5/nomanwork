package com.nexus.navigationdrawer.Fragments

import android.app.Activity
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.nexus.navigationdrawer.R
import com.nexus.navigationdrawer.Util.RecyclerItemClickListenr
import com.nexus.recyclerviewkotlin.MainAdapter
import com.nexus.recyclerviewkotlin.Model_Video
import kotlinx.android.synthetic.main.fr_menu_1.*
import kotlinx.android.synthetic.main.fr_menu_1.view.*
import kotlinx.android.synthetic.main.fragment_downloads.*
import kotlinx.android.synthetic.main.fragment_downloads.view.*
import kotlinx.android.synthetic.main.fragment_video.*

class DownloadsFragment : Fragment(){


    internal var root: View? = null

    var activity1:Activity?=null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        root= inflater.inflate(R.layout.fragment_downloads, container, false)
        activity1=requireActivity()
        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //you can set the title for your toolbar here for different fragments different titles
        activity!!.title=resources.getString(R.string.download_frag_title)
        get_videos()
    }

    //function for get videos from gallery

    fun get_videos() {
        val staggeredGridLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        root!!.recyclerView_main.layoutManager = staggeredGridLayoutManager

        val int_position = 0
        val uri: Uri
        val cursor: Cursor?
        val column_index_data: Int
        val column_index_folder_name: Int
        val column_id: Int
        val thum: Int

        var absolutePathOfImage: String? = null
        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI

        val projection = arrayOf(
            MediaStore.MediaColumns.DATA,
            MediaStore.Video.Media.BUCKET_DISPLAY_NAME,
            MediaStore.Video.Media._ID,
            MediaStore.Video.Thumbnails.DATA
        )

        val orderBy = MediaStore.Images.Media.DATE_TAKEN
        cursor = requireContext().applicationContext.contentResolver.query(
            uri, projection, null, null,
            "$orderBy DESC"
        )

        column_index_data = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
        column_index_folder_name =
            cursor!!.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_DISPLAY_NAME)
        column_id = cursor!!.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
        thum = cursor!!.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA)

        val videoList = ArrayList<Model_Video>()

        while (cursor!!.moveToNext()) {
            absolutePathOfImage = cursor!!.getString(column_index_data)
            Log.e("Column", absolutePathOfImage)
            Log.e("Folder", cursor!!.getString(column_index_folder_name))
            Log.e("column_id", cursor!!.getString(column_id))
            Log.e("thum", cursor!!.getString(thum))

            val obj_model = Model_Video(absolutePathOfImage,cursor!!.getString(column_id),false)

            videoList.add(obj_model)


        }
        val setAdapter = MainAdapter(activity1, videoList)
        recyclerView_main.adapter = setAdapter

        recyclerView_main.addOnItemTouchListener(RecyclerItemClickListenr(requireContext(), recyclerView_main, object : RecyclerItemClickListenr.OnItemClickListener {

            override fun onItemClick(view: View, position: Int) {
          val videoFragment = VideoFragment()
           val args = Bundle()
            val video:   Model_Video=  videoList[position]
            args.putString("path",video.str_path)
            videoFragment.setArguments(args)
            val transaction = fragmentManager!!.beginTransaction()
            transaction.replace(

               R.id.nav_host_fragment,
               videoFragment
             ) // give your fragment container id in first parameter
            transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
           transaction.commit()



                //do your work here..
            }
            override fun onItemLongClick(view: View?, position: Int) {
                TODO("do nothing")
            }
        }))

    }

}