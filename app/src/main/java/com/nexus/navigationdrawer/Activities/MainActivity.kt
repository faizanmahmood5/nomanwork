package com.nexus.navigationdrawer.Activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.nexus.navigationdrawer.R
import android.graphics.drawable.ColorDrawable
import com.nexus.navigationdrawer.Fragments.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import java.util.*
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.content.res.Configuration
import android.content.res.Resources
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.core.graphics.drawable.toDrawable
import com.nexus.navigationdrawer.Helper.LocaleHelper
import io.paperdb.Paper
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*


public class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var menuMain: Menu
    override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

        /*val languageToLoad = "fa" // your language
        val locale = Locale(languageToLoad)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )
        */
    setContentView(R.layout.activity_main)

    val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
    setSupportActionBar(toolbar)


    val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
    val toggle = ActionBarDrawerToggle(
        this, drawer, toolbar,
        R.string.navigation_drawer_open,
        R.string.navigation_drawer_close
    )
    drawer.addDrawerListener(toggle)
    toggle.syncState()

    val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
    navigationView.setNavigationItemSelectedListener(this)

    //displaySelectedScreen(R.id.nav_home)
    displaySelectedScreen(R.id.nav_home)
    val sharedPreference:SharedPreference=SharedPreference(this)
    changeToTheme(sharedPreference.getValueInt("Theme"))

        /*Paper.init(this)
        var language:String?=Paper.book().read<String>("language")
        if (language!=null)
        {
            //Paper.book().write("language","ur")
            //updateView(Paper.book().read<String>("language"))
        }
*/
}// onCreate end bracket

    public fun updateView(lang: String) {
        var context:Context=LocaleHelper.setLocale(this,lang)
        var resources:Resources=context.resources
        //val appName = findViewById<View>(R.id.app_name) as TextView
        //appName.text=(resources.getString(R.string.app_name))
        app_name?.text=resources.getString(R.string.app_name)
        textView?.text=resources.getString(R.string.nav_header_subtitle)

        var item:MenuItem=menuMain.findItem(R.id.nav_home)
        item.title=resources.getString(R.string.menu_home)
       // R.id.nav_home?.text=resources.getString(R.string.nav_header_subtitle)
    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }

        val fragment =
            this.supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        (fragment as? IOnBackPressed)?.onBackPressed()?.not()?.let {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        menuMain=menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        when (id) {
            R.id.first_category -> {
                Paper.book().write("category", "first_cat")
                item.isChecked = !item.isChecked
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.nav_host_fragment, HomeFragment())
                ft.commit()
              //  restartActivity(this)
                return true
            }
            R.id.second_category -> {
                Paper.book().write("category", "second_cat")
                item.isChecked = !item.isChecked
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.nav_host_fragment, HomeFragment())
                ft.commit()
               // restartActivity(this)
                return true
            }
            R.id.third_category -> {
                Paper.book().write("category", "third_cat")
                item.isChecked = !item.isChecked
               /// restartActivity(this)
                val ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.nav_host_fragment, HomeFragment())
                ft.commit()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
        return false
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        displaySelectedScreen(item.itemId)

        return true
    }


    private fun displaySelectedScreen(itemId: Int) {

        //creating fragment object
        var fragment: Fragment? = null

        //initializing the fragment object which is selected
        when (itemId) {
            R.id.nav_home -> fragment = HomeFragment()
            R.id.nav_about -> fragment = AboutFragment()
            R.id.nav_settings -> fragment = SettingsFragment()
            R.id.nav_downloads -> fragment = DownloadsFragment()
            R.id.nav_tabs -> fragment = TabsFragment()
            R.id.nav_share -> showToast("Share Icon pressed")
            R.id.nav_more_apps -> showToast("More Apps Icon pressed")
        }

        //replacing the fragment
        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.nav_host_fragment, fragment)
            ft.commit()
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
    }

    fun showToast(message: String)
    {
        Toast.makeText(applicationContext,message, Toast.LENGTH_SHORT).show()
    }


    interface IOnBackPressed {
        fun onBackPressed(): Boolean
    }

    val THEME_DEFAULT = 0
    val THEME_BLUE = 1
    val THEME_PURPULE = 2
    val THEME_GREEN = 3
    val THEME_RED = 4
    val THEME_ORANGE = 5
    val THEME_BROWN = 6
    val THEME_MAROON = 7
    val THEME_PLUM = 8
    /**
     * Set the theme of the Activity, and restart it by creating a new Activity of the same type.
     */
    fun changeToTheme(theme: Int) {

        when (theme) {
            THEME_DEFAULT ->
            {
                setTheme(R.style.AppTheme)
            }
            THEME_BLUE ->
            {
                setTheme(R.style.blue_theme)
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorBlue)))
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(true)
            }
            THEME_PURPULE ->
            {
                setTheme(R.style.purple_theme)
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorPlum)))
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(true)

            }
            THEME_GREEN ->
            {
                setTheme(R.style.green_theme)
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorGreen)))
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(true)
            }
            THEME_RED ->
            {
                setTheme(R.style.red_theme)
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorRed)))
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(true)
            }
            THEME_ORANGE ->
            {
                setTheme(R.style.orange_theme)
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorOrange)))
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(true)
            }
            THEME_BROWN ->
            {
                setTheme(R.style.brown_theme)
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorBrown)))
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(true)
            }
            THEME_MAROON ->
            {
                setTheme(R.style.maroon_theme)
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorMaroon)))
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(true)
            }
            THEME_PLUM ->
            {
                setTheme(R.style.plum_theme)
                supportActionBar!!.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorPlum)))
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                supportActionBar!!.setDisplayShowTitleEnabled(true)
            }
            else -> setTheme(R.style.AppTheme)
        }



    }

    fun restartActivity(activity: Activity)
    {
/*
        activity.finish()
        activity.startActivity(Intent(activity, activity.javaClass))
*/
        //activity.recreate()
        val i = intent
        activity.overridePendingTransition(0, 0)
//        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        activity.finish()
        //restart the activity without animation
        activity.overridePendingTransition(0, 0)
        //activity.startActivity(i)
        activity.startActivity(Intent(activity,activity.javaClass))
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(com.nexus.navigationdrawer.Activities.LocaleHelper.onAttach(newBase,"en"))
    }

}
