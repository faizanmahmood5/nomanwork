package com.nexus.navigationdrawer.Util

import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.content.Intent.getIntent
import androidx.core.app.ActivityCompat.recreate
import android.os.Build
import android.app.Activity
import android.annotation.SuppressLint



class GlobalClass
{

    companion object
    {
        @SuppressLint("NewApi")
        fun recreateActivityCompat(a: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                a.recreate()
            } else {
                val intent = a.intent
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                a.finish()
                a.overridePendingTransition(0, 0)
                a.startActivity(intent)
                a.overridePendingTransition(0, 0)
            }
        }
    }

}