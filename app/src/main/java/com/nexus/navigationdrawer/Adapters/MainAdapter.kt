package com.nexus.recyclerviewkotlin

import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.nexus.navigationdrawer.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_row.view.*
import java.lang.Long.parseLong
import android.os.Bundle
import com.nexus.navigationdrawer.Fragments.VideoFragment
import androidx.appcompat.app.AppCompatActivity
import com.nexus.navigationdrawer.Fragments.DownloadsFragment


class MainAdapter(var c: Activity?, val images: ArrayList<Model_Video>) : RecyclerView.Adapter<MainAdapter.CustomViewHolder>() {


    override fun getItemCount(): Int {
        return images.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return MainAdapter.CustomViewHolder(
            LayoutInflater.from(c).inflate(
                R.layout.layout_row,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val path = images[position]

        Picasso.get()
            .load(ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, parseLong(path.str_thumb)))
            .resize(250, 250)
            .centerCrop()
            .into(holder.iv)

//        holder.iv.setOnClickListener {
//            //handle click event on thumbnail
///*            val intent = Intent(it.context, VideoViewActivity::class.java)
//            intent.putExtra("path",path.str_path)
//            it.context.startActivity(intent)
//
//
// */
////            val downloadsFragment=DownloadsFragment()
////            val videoFragment = VideoFragment()
////            val args = Bundle()
////            args.putString("path",path.str_path)
////            videoFragment.setArguments(args)
////            val transaction = c?.fragmentManager!!.beginTransaction()
////            transaction.replace(
////                R.id.nav_host_fragment,
////                videoFragment
////            ) // give your fragment container id in first parameter
////            transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
////            transaction.commit()
//
//        }
    }

    /*
class CustomViewHolder(val view: View): RecyclerView.ViewHolder(view)
{

}
 */
    class CustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val iv = view.thumbnail as ImageView
    }
}